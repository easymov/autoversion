#!/bin/bash

set -e

echo "==================="
echo " AutoVersion 0.4.0 "
echo "==================="

git=git
makaron=makaron
choukette=choukette
RELEASE_BRANCH=master

function debug () {
  if [ -z "$CI_COMMIT_REF_NAME" ]; then
      CI_COMMIT_REF_NAME=$(git symbolic-ref --short HEAD)
  fi

  if [ -z "$CI_COMMIT_SHA" ]; then
      CI_COMMIT_SHA=$(git rev-parse HEAD)
  fi

  if [ -z "$CI_REPOSITORY_URL" ]; then
      CI_REPOSITORY_URL=$(git config --get remote.origin.url)
  fi

  git="echo # git"
  function bash() { read stdin; echo \# $stdin; }
  makaron="echo # makaron"
}

# handling options
while getopts "b:d" opt; do
  case $opt in
    b)
      RELEASE_BRANCH=$OPTARG
      ;;
    d)
      debug
      ;;
    \?)
      echo "Invalid option: -$OPTARG" >&2
      exit 1
      ;;
    :)
      echo "Option -$OPTARG requires an argument." >&2
      exit 1
      ;;
  esac
done

if [ -v CI_COMMIT_TAG ]; then
  echo "CI on tag: doing nothing"
  exit 0
fi

echo "Version: $(makaron)"
choukette version $(makaron) 007ec6 > version.svg

function configure_git () {
  echo "Configure git"
  $git config --global user.email "$GITLAB_USER_EMAIL"
  $git config --global user.name "$GITLAB_USER_NAME"
  local CI_PUSH_REPO=$(echo "$CI_REPOSITORY_URL" | perl -pe 's#.*@(.+?(\:\d+)?)/#git@\1:#')
  $git remote set-url --push origin "${CI_PUSH_REPO}"
}

function clean_git () {
  echo "Clean git"
  git branch | grep -o "version-.*" | sed 's/^[ *]*//' | sed 's/^/git branch -D /' | bash || true
}

configure_git
clean_git

COMMIT_MSG=$(git log -1 --pretty=%B)
RELEASE=$(([[ $COMMIT_MSG =~ \[(major)\] ]] && echo major) || ([[ $COMMIT_MSG =~ \[(minor)\] ]] && echo minor) || ([[ $COMMIT_MSG =~ \[(patch)\] ]] && echo patch) || echo none)

echo "Requested release: $RELEASE"

if [ "$CI_COMMIT_REF_NAME" = "$RELEASE_BRANCH" ]; then

  if [ "$RELEASE" != "none" ]; then
    echo ">> Release on $RELEASE_BRANCH"
    # COMMIT AND PUSH
    NEXT=$(makaron "$RELEASE" --next)
    echo "Version will be $NEXT"
    $makaron --apply "$NEXT" --yes
    $git checkout "$CI_COMMIT_REF_NAME"
    $git commit -am "Upgrade version to $NEXT [skip ci]"
    $git tag $NEXT
    $git push  origin "$CI_COMMIT_REF_NAME"
    $git push --tag
  else
    echo ">> Tagging version $(makaron)"
    $git tag $(makaron)
    $git push --tag
  fi

else
  if [ "$RELEASE" != "none" ]; then
    echo ">> Release feature "$CI_COMMIT_REF_NAME" on $RELEASE_BRANCH"
    # UPDATE VERSION
    $git checkout "$RELEASE_BRANCH"
    $git pull origin "$RELEASE_BRANCH"
    NEXT=$(makaron "$RELEASE" --next)
    echo "Version will be $NEXT"
    BRANCH_NAME=version-$NEXT-$CI_COMMIT_SHA
    echo "New branch will be $BRANCH_NAME"
    $git checkout "$CI_COMMIT_SHA"
    $git checkout -b "$BRANCH_NAME"

    # COMMIT AND PUSH
    $makaron --apply "$NEXT" --yes
    $git commit -am "Upgrade version to $(makaron) [skip ci]"
    $git push origin "$BRANCH_NAME"

  else
    echo "Nothing to do."
  fi
fi

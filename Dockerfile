FROM nmartignoni/makaron
MAINTAINER dev@easymov.fr
RUN apk --update add git openssh perl bash curl && rm -rf /var/cache/apk/*
RUN pip install choukette
ADD autoversion.sh /bin
